import plugins.funcs as funcs

commands = {
    "add" : funcs.add_song,
    "current" : funcs.current_song,
    "link" : funcs.get_streaming_url
}
