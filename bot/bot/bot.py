from os import environ
from twisted.internet import protocol, reactor
from twisted.words.protocols import irc
from datetime import datetime
# from logging import getLogger
import logging

from plugins.commands import commands

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

class Bot(irc.IRCClient):

    nickname = environ['NICKNAME']
    password = environ['PASSWORD']

    def signedOn(self):
        self.join(self.factory.channel, self.factory.key)

    def joined(self, channel):
        print(datetime.utcnow().isoformat() + " - joined: ", channel)

    def privmsg(self, user, channel, msg):
        user = user.split('!', 1)[0]

        # incoming privmsg, reroute it back to the original sender.
        if channel == self.nickname:
            channel = user

        if msg.startswith('<>'):
            command = msg.split(' ')[1]

            if command in commands:
                commands[command](self, channel, msg)


class BotFactory(protocol.ClientFactory):
    def __init__(self, channel, key):
        self.channel = channel
        self.key = key
    def buildProtocol(self,addr):
        p = Bot()
        p.factory = self
        return p

    def clientConnectionLost(self,connector,reason):
        connector.connect()

    def clientConnectionFailed(self,connector,reason):
        print("connection failed: ", reason)
        reactor.stop()
