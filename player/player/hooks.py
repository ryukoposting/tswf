from player import stream_song
from os import remove
from requests import post
import ntpath

def download_hook(d):
    if d['status'] == 'finished':
        post("http://127.0.0.1:8080/api/previous/" + ntpath.basename(d['filename']))
        stream_song(d['filename'])
